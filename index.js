// Set up dependencies

const mongoose = require('mongoose');
const express = require('express');
const productRoutes = require('./routes/productRoutes.js');
const userRoutes = require('./routes/userRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');

const cors = require('cors');

// Set up server

const app = express();

// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// Database connection

mongoose.connect(
	'mongodb+srv://admin:admin1234@b256morante.1ywoapf.mongodb.net/capstone3?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

// Routes
app.use('/users', userRoutes);
app.use('/orders', orderRoutes);
app.use('/product', productRoutes);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log('We are connected to the database'));

// Server listening

app.listen(4000, () => console.log('API is now online on port 4000'));
