const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId: {
		type: mongoose.SchemaTypes.ObjectId,
		ref: 'User',
		required: true,
	},
	products: [
		{
			productId: {
				type: mongoose.SchemaTypes.ObjectId,
				ref: 'Product',
				required: true,
			},
			quantity: {
				type: Number,
				required: [true, 'quantity is required'],
			},
		},
	],

	totalAmount: {
		type: Number,
	},

	purchasedOn: {
		type: Date,
		default: new Date(),
	},
});

module.exports = mongoose.model('Order', orderSchema);
