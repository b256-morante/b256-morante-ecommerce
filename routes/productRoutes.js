const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController.js');
const auth = require('../auth.js');

// Creating a new product

router.post('/create', auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	};

	if (data.isAdmin) {
		productController
			.createProduct(data.product)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

// Route for retrieving all products

router.get('/all', (req, res) => {
	productController
		.getAllProducts()
		.then((resultFromController) => res.send(resultFromController));
});

// Route for active products
router.get('/active', (req, res) => {
	productController
		.getActiveProducts()
		.then((resultFromController) => res.send(resultFromController));
});

// Route for retrieving specific product

router.get('/:productId', (req, res) => {
	productController
		.getProduct(req.params)
		.then((resultFromController) => res.send(resultFromController));
});

// Route for updating a specific product

router.put('/update/:productId', auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params,
	};

	if (data.isAdmin) {
		productController
			.updateProduct(data.product, data.params)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

// Archiving a course

router.put('/archive/:productId', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params,
	};

	if (data.isAdmin) {
		productController
			.archiveProduct(data.params)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

// reArchiving a course

router.put('/reactivate/:productId', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params,
	};

	if (data.isAdmin) {
		productController
			.rearchiveProduct(data.params)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		res.send(false);
	}
});
module.exports = router;
