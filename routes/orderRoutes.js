const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController.js');
const auth = require('../auth.js');
const jwt = require('jsonwebtoken');
const secret = 'SecretBooking';

router.post('/create', auth.verify, async (req, res) => {
	try {
		const user = auth.decode(req.headers.authorization);

		const userId = user._id;

		console.log(user);

		if (!user.isAdmin) {
			const resultFromController = await orderController.createOrder(
				req.body,
				userId
			);
			res.send(resultFromController);
		} else {
			res.send(false);
		}
	} catch (error) {
		res.status(500).send(error.message);
	}
});

// Router to get all order by admin
router.get('/allOrder', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params,
	};

	if (data.isAdmin) {
		orderController
			.getAllOrder()
			.then((resultFromController) => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

// Router to get order by User

router.get('/myOrder', auth.verify, async (req, res) => {
	try {
		const user = auth.decode(req.headers.authorization);

		const userId = user._id;

		const resultFromController = await orderController.getUserOrder(userId);

		res.send(resultFromController);
	} catch (error) {
		res.status(500).send(error.message);
	}
});

module.exports = router;
