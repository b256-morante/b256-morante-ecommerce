const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const userController = require('../controllers/userController.js');

// Route for creating a user

router.post('/register', (req, res) => {
	userController
		.registerUser(req.body)
		.then((resultFromController) => res.send(resultFromController));
});

// Route for authenticating user

router.post('/login', (req, res) => {
	userController
		.authenticateUser(req.body)
		.then((resultFromController) => res.send(resultFromController));
});

// Promote User

router.put('/promote/:userId', auth.verify, (req, res) => {
	const data = {
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params,
	};
	if (data.isAdmin) {
		userController
			.promoteUser(data.user, data.params)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

// Route for getting user details

router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	userController
		.getProfile({ userId: userData._id })
		.then((resultFromController) => res.send(resultFromController));

	console.log(userData);
});

module.exports = router;
