const Product = require('../models/product.js');

// Creating new product

module.exports.createProduct = (product) => {
	let newProduct = new Product({
		product: product.product,
		description: product.description,
		price: product.price,
	});

	return newProduct.save().then((result, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	});
};

// Retrieve all products

module.exports.getAllProducts = () => {
	return Product.find({}).then((result) => {
		return result;
	});
};

// Retrieving all active products

module.exports.getActiveProducts = () => {
	return Product.find({ isActive: true }).then((result) => {
		return result;
	});
};

// Retrieving a Specific Product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then((result) => {
		return result;
	});
};

// Updating a specific product

module.exports.updateProduct = (product, paramsId) => {
	let updatedProduct = {
		product: product.product,
		description: product.description,
		price: product.price,
	};

	return Product.findByIdAndUpdate(paramsId.productId, updatedProduct).then(
		(result, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		}
	);
};

// Archiving a Product

module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false,
	};

	return Product.findByIdAndUpdate(
		reqParams.productId,
		updateActiveField
	).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

// ReArchiving a Product

module.exports.rearchiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: true,
	};

	return Product.findByIdAndUpdate(
		reqParams.productId,
		updateActiveField
	).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};
