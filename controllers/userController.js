const User = require('../models/user.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

module.exports.registerUser = async (user) => {
	const duplicateUser = await User.findOne({ email: user.email });

	if (duplicateUser) {
		return { error: 'email is existing' };
	} else {
		let newUser = new User({
			email: user.email,
			password: user.password,
			isAdmin: user.isAdmin,

			password: bcrypt.hashSync(user.password, 10),
		});

		return newUser.save().then((result, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		});
	}
};

// Authenticate User

module.exports.authenticateUser = (user) => {
	return User.findOne({ email: user.email }).then((result) => {
		if (result == null) {
			return { Error: 'Not Found' };
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				user.password,
				result.password
			);

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) };
			} else {
				return false;
			}
		}
	});
};

// Set user as admin

module.exports.promoteUser = (user, paramsId) => {
	let updatedUser = {
		email: user.email,
		password: user.password,
		isAdmin: user.isAdmin,
	};
	return User.findByIdAndUpdate(paramsId.userId, updatedUser).then(
		(result, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		}
	);
};

// Get user details

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then((result) => {
		result.password = '';

		return result;
	});
};
