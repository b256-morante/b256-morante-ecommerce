const Order = require('../models/order.js');
const User = require('../models/user.js');
const auth = require('../auth.js');
const jwt = require('jsonwebtoken');
const secret = 'SecretBooking';
const Product = require('../models/product.js');
// Create an order

const createOrder = async (order, userId) => {
	let productPrices = [];
	const products = order.products;
	let totalAmount = 0;
	for (let i = 0; i < products.length; i++) {
		const product = await Product.findById(products[i].productId);
		productPrices.push(product.price);
		totalAmount += products[i].quantity * productPrices[i];
	}
	try {
		// Create a new order document using the Order model
		const newOrder = new Order({
			userId: userId,
			products: order.products,
			totalAmount: totalAmount,
		});

		console.log(newOrder);
		// Save the new order to the database
		const result = await newOrder.save();

		return result;
	} catch (error) {
		throw new Error(error.message);
	}
};

module.exports = { createOrder };

// Retrieve All Order

module.exports.getAllOrder = () => {
	return Order.find({}).then((result) => {
		return result;
	});
};

// Retrieve Order of User

module.exports.getUserOrder = (userId) => {
	return Order.find({ userId }).then((result) => {
		console.log('userId', userId);
		return result;
	});
};

// Adding order
/*
module.exports.updateCheckOut = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then((user) => {
		user.product.push({ userId: data.userId });

		return user.save().then((registered, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		});
	});

	let isProductUpdated = await Product.findById(data.userId).then(
		(product) => {
			product.product.push({ userId: data.userId });

			return product.save().then((addToCart, err) => {
				if (err) {
					return false;
				} else {
					return true;
				}
			});
		}
	);

	if (isUserUpdated && isProductUpdated) {
		return true;
	} else {
		return false;
	}
};
*/
